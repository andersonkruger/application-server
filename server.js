/**
 * Mobile backend RESTful server
 *
 */

// dependencies
var express = require('express');
var mongoose = require('mongoose');
var parser = require('body-parser');
var passport = require('passport');
var multer = require('multer');
var port = process.env.PORT || 8080;
// server definition
var server = express();

var tempDIR = process.env.TEMP_DIR || './temp/';

server.use(parser.json());
server.use(multer({dest: tempDIR}).array('arquivo'));
server.use(parser.urlencoded({extended:true}));
server.use(passport.initialize());

var http = server.listen(port)
// var io = require('socket.io').listen(http);
mongoose.connect('mongodb://localhost:27017/application-server');


// routes
var routerV1 = express.Router();
var publicRouterV1 = express.Router();
var AppAuthController = require('./core/Controller/AuthController');

var dns = process.env.DNS || 'api.simplesgov.com.br';

/**
 * Applications
 *
 * Permitir a criação de aplicacoes que podem acessar o rest por OAuth2
 *
 */
var ApplicationController = require('./core/Controller/ApplicationController');
// routerV1.route('/application/new').post(ApplicationController.new);

var ContentController = require('./core/Controller/ContentController');
routerV1.route('/save-file').post(AppAuthController.isAuth,ContentController.receiveFile);
publicRouterV1.route('/view-file/:codigo/:name').get(ContentController.serveFile);

/**
 * OAuth2
 *
 * Controle de autorizacao e token para as aplicacoes
 */
var OAuth2Controller = require('./core/Controller/OAuth2Controller');
routerV1.route('/oauth2/token').post(OAuth2Controller.token);

/**
 * Usuarios
 *
 */
var UsuarioController = require('./app/Controller/UsuarioController');

routerV1.route('/usuario/save').post(AppAuthController.isAuth, UsuarioController.new);
routerV1.route('/usuario/list').get(AppAuthController.isAuth, UsuarioController.findAll);
routerV1.route('/usuario/find/:codigo').get(AppAuthController.isAuth, UsuarioController.find);
routerV1.route('/usuario/save/:codigo').put(AppAuthController.isAuth, UsuarioController.edit);
routerV1.route('/usuario/remove/:codigo').delete(AppAuthController.isAuth, UsuarioController.delete);

/**
 * Ocorrencias
 */
var OcorrenciaController = require('./app/Controller/OcorrenciaController');

routerV1.route('/ocorrencia/save').post(AppAuthController.isAuth, function(req, res,next) {
    OcorrenciaController.new(req, res, next,io);
});
routerV1.route('/ocorrencia/find/:codigo').get(AppAuthController.isAuth, OcorrenciaController.find);
routerV1.route('/ocorrencia/list').get(AppAuthController.isAuth, OcorrenciaController.findAll);
routerV1.route('/ocorrencia/clean').get(OcorrenciaController.cleanUp);


server.use('/api/v1', routerV1);
server.use('/', publicRouterV1);
server.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "http://livemap.simplesgov.com.br");
    res.header('Access-Control-Allow-Credentials', true);
    next();
});

// io.sockets.on('connection', function (socket) {
//     var Ocorrencia = require('./app/Model/Ocorrencia');
//     Ocorrencia.find(function(err,retorno) {
//         if (err) {
//             return res.send(err);
//         }
//         socket.emit('all:ocorrencias',retorno);
//     });
// });

console.log('Servidor iniciado em '+dns+':'+port+'!');
