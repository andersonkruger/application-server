# Node APP Server #

Projeto base de backend em Node.JS para mobile apps.

### Funcionalidades (iniciais)

* RESTful Routes (Express)
* Persistência de dados (MongoDB/Mongoose)
* Controle de Acesso (OAuth 2.0/client-credentials)
* File Upload (Multer)
* Modelos de CRUD (Ocorrências/Usuários/...)

### Configuração ###

#### Setup ####

* Node.JS
* NPM
* MongoDB

Atualizar as dependências: 

```
$ npm install
```

Iniciar serviço:

```
$ node server.js
```

##### Ambiente de desenvolvimento #####

* NodeMon - Monitora os arquivos do projeto para reiniciar o servidor a cada alteração.

``` 
$ npm install -g nodemon
$ nodemon server.js
```

##### EM PRODUÇÃO #####

* forever - Mantém o serviço em execução.

``` 
$ npm install -g forever
$ forever start server.js
```

#### Sugestões ####

Anderson Krüger - <andersonkruger@gmail.com>