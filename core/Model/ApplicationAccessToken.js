var mongoose = require('mongoose');
var uuid = require('uuid');

var ApplicationAccessTokenSchema = new mongoose.Schema({
    token: {type: String, required: true, default: uuid.v4() },
    expirationDate: {type: Date, required: true, default: Date.now },
    applicationId: {type: String, unique: true, required: true}
},{collection:'CORE-ApplicationAccessTokens'});

module.exports = mongoose.model('ApplicationAccessToken', ApplicationAccessTokenSchema);
