var mongoose = require('mongoose');
var uuid = require('uuid');

var ApplicationSchema = new mongoose.Schema({
    nome: {type: String, unique: true, required: true, trim: true},
    descricao: {type: String, trim: true },
    responsavel: {type: String, required: true, trim: true},
    id: {type: String, unique: true, required: true, default: uuid.v4() },
    secret: {type: String, unique: true, required: true, default: uuid.v4() }
},{collection:'CORE-Applications'});

module.exports = mongoose.model('Application', ApplicationSchema);
