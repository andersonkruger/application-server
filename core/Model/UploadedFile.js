var mongoose = require('mongoose');

var UploadedFileSchema = new mongoose.Schema({
    nome: {type: String, unique: true, required: true, trim: true},
    nomeFisico: {type: String, unique: true, required: true, trim: true},
    mimeType: {type: String, required: true, trim: true},
    data: {type: Date, required: true, default: Date.now},
    tamanho: {type: Number, required: true}
},{collection:'CORE-UploadedFiles'});

module.exports = mongoose.model('UploadedFile', UploadedFileSchema);
