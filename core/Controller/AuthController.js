var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
// basic user-auth
var Usuario = require('../../app/Model/Usuario');
// basic app-auth (getAuthorization,getToken)
var Application = require('../Model/Application');
var ApplicationAccessToken = require('../Model/ApplicationAccessToken');


passport.use('app-secret-basic-auth', new BasicStrategy(
    function (appID, appSecret, next) {
        Application.findOne({
            id: appID
        }, function (err, model) {
            if (err) {
                next(err);
            }
            if (!model || model.secret !== appSecret) {
                return next(null, false);
            }
            return next(null, model);
        });
    }
));

passport.use('app-secret-passw-auth', new ClientPasswordStrategy(
    function (appID, appSecret, next) {
        console.log(1);
        Application.findOne({
            id: appID
        }, function (err, model) {
            if (err) {
                next(err);
            }

            if (!model || model.secret !== appSecret) {
                return next(null, false);
            }
            return next(null, model);
        });
    }
));

passport.use("token-auth", new BearerStrategy(
    function (token, next) {

        ApplicationAccessToken.findOne({
            token: token
        }, function (err, model) {
            if (err) {
                next(err);
            }
            if (!model) {
                return next(null, false);
            }
            if (new Date() > model.expirationDate) {
                model.remove(function (err) {
                    if (err) {
                        next(err);
                    }
                });
            } else {
                Application.findOne({
                    id: model.applicationId
                },function(err,appModel) {
                    if (!appModel) {
                        return next(null, false);
                    }
                    return next(null, appModel,{ scope: '*' });
                });
            }
        });
    }
));

exports.isAuth = passport.authenticate('token-auth', {session: false});
