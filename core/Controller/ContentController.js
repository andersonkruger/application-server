var UploadedFile = require('../../core/Model/UploadedFile');
var fileSystem = require('fs');
var path = require('path');

var uploadDIR = process.env.CLOUD_DIR || './data/uploaded-files/';

var saveFileLocal = function(arquivo) {
    var destinationPath = path.join(uploadDIR,arquivo.filename);
    var source = fileSystem.createReadStream(arquivo.path);
    var dest = fileSystem.createWriteStream(destinationPath);

    source.pipe(dest);

    source.on('end', function() {
    //    console.log('Arquivo salvo em "'+destinationPath+'".');
    });
    source.on('error', function(err) {
    //    console.log('Não foi possível salvar o arquivo: ',err)
        console.log(err);
    });

    var file = new UploadedFile({
        nome: arquivo.originalname,
        nomeFisico: arquivo.filename,
        mimeType: arquivo.mimetype,
        tamanho: arquivo.size
    });

    file.save(function(err) {
        if (err) {
            res.send(err);
        }
    });
    fileSystem.unlink(arquivo.path);
    return file;
};

exports.receiveFile = function(req,res) {
    var arquivosRecebidos = [];

    if (req.files && Object.keys(req.files).length) {
        for(var i in req.files) {
            arquivosRecebidos.push(saveFileLocal(req.files[i]));
        }
    }
    res.json(arquivosRecebidos);
};

exports.serveFile = function(req, res) {
    UploadedFile.findById(req.params.codigo, function(err,model) {
        if (err) {
            res.json({'msg':'Arquivo não encontrado!'});
        }
        if (model) {

            var filePath = path.join(uploadDIR,model.filename);
            res.setHeader('Content-disposition', 'attachment; filename=' + model.nome);
            res.setHeader('Content-type', model.mimeType);
            fileSystem.createReadStream(filePath).pipe(res);
        }
    });


};

