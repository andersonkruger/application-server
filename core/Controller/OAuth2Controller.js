var OAuth2 = require('oauth2orize');
var uuid = require('uuid');
var passport = require('passport');

var ApplicationAccessToken = require('../Model/ApplicationAccessToken');

var server = OAuth2.createServer();

server.exchange (

    OAuth2.exchange.clientCredentials(function(app, scope, next) {
        var tokenHash = uuid.v4();
        //var tokenHash = crypto.createHash('sha1').update(token).digest('hex');
        var expiresIn = 1800;
        var expirationDate = new Date(new Date().getTime() + (expiresIn * 1000))

        ApplicationAccessToken.findOne({
            applicationId: app.id
        },function(err,model) {
            if (err) {
                next(err);
            }

            if (!model) {
                model = new ApplicationAccessToken({
                    token: tokenHash,
                    applicationId: app.id,
                    expirationDate: expirationDate
                });
            }
            if (new Date() > model.expirationDate) {
                model.expirationDate = expirationDate;
            }
            model.save(function(err) {
                if (err) {
                    return next(err);
                }
                return next(null,model,{expires_in:expiresIn});
            })
        });


    }));

exports.token = [
    passport.authenticate(['app-secret-basic-auth', 'app-secret-passw-auth'], { session: false }),
    server.token(),
    server.errorHandler()
];
