var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var UsuarioSchema = new mongoose.Schema({
    nome: {type: String, required: true},
    email: {type: String, unique: true, required: true},
    senha: {type: String, required: true}
});

UsuarioSchema.pre('save', function(next) {
    var usuario = this;

    // se a senha não alterou
    if (!usuario.isModified('senha')) {
        return next();
    }

    bcrypt.genSalt(5, function(err,salt) {
        if (err) {
            return next(err);
        }

        bcrypt.hash(usuario.senha, salt, null, function(err,hash) {
            if (err) {
                return next(err);
            }
            usuario.senha = hash;
            next();
        });
    });
});

/**
 * Verifica se e valida uma senha passada em relacao a do cadastro..
 *
 * @param senha String
 * @param next Function (callback)
 */
UsuarioSchema.methods.verificaSenha = function(senha, next) {
    bcrypt.compare(senha, this.senha, function(err,isMatch) {
        if (err) {
            return next(err);
        }
        next(null,isMatch);
    });
};

module.exports = mongoose.model('Usuario', UsuarioSchema);
