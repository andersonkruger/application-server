var mongoose = require('mongoose');

var OcorrenciaSchema = new mongoose.Schema({
    tipo: {type: Number, min: 1, max: 7 , required: true},
    descricao: {type: String, trim: true, required: true},
    data: {type: Date, required: true, default: Date.now },
    endereco: {type: String, trim: true, required: true},
    cep: {type: String, trim: true, required: true},
    cidadeId: {type: String, trim: true, required: true},
    local: {type:[Number],index:'2d'},
    arquivos: {type: []}
});

module.exports = mongoose.model('Ocorrencia', OcorrenciaSchema);
