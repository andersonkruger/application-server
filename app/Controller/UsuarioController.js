var Usuario = require('../Model/Usuario');

exports.new = function(req, res) {
    var model = new Usuario();
    model.nome = req.body.nome;
    model.email = req.body.email;
    model.senha = req.body.senha;

    model.save(function(err) {
        if (err) {
            res.send(err);
        }
        res.json({message:'OK',data:model});
    });
};

exports.edit = function(req, res) {
    Usuario.findById(req.params.codigo, function(err,model) {
        if (err) {
            res.send(err);
        }
        model.nome = req.body.nome;
        model.email = req.body.email;
        model.senha = req.body.senha;
        model.save(function(err) {
            if (err) {
                res.send(err);
            }
            res.json(model);
        });
    });
};

exports.delete = function(req, res) {
    Usuario.findByIdAndRemove(req.params.codigo, function(err) {
        if (err)
            res.send(err);

        res.json({ message: 'REMOVED' });
    });
};

exports.find = function(req, res) {
    Usuario.findById(req.params.codigo, function(err,model) {
        if (err) {
            res.send(err);
        }
        res.json(model);
    });
};

exports.findAll = function(req, res) {
    Usuario.find(function(err,retorno) {
        if (err) {
            return res.send(err);
        }
        res.json(retorno);
    });
};


