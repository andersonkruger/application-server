var Ocorrencia = require('../Model/Ocorrencia');


exports.new = function(req, res,next,io) {
    var model = new Ocorrencia({
        tipo: req.body.tipo,
        descricao: req.body.descricao,
        endereco: req.body.endereco,
        cep: req.body.cep,
        cidadeId: req.body.cidade,
        local: [req.body.longitude||0, req.body.latitude||0],
        arquivos: req.body.arquivos
    });
    model.save(function(err) {
        if (err) {
            res.send(err);
        }
        res.json({message:'OK',data:model});
        // mandando para o livemap a nova ocorrencia
        io.sockets.emit('new:ocorrencia', model);
    });
};

exports.find = function(req, res) {
    Ocorrencia.findById(req.params.codigo, function(err,model) {
        if (err) {
            res.send(err);
        }
        res.json(model);
    });
};

exports.cleanUp = function(req, res) {
        Ocorrencia.remove().exec(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'REMOVED' });
        });
};

exports.findAll = function(req, res) {
    Ocorrencia.find(function(err,retorno) {
        if (err) {
            return res.send(err);
        }
        res.json(retorno);
    });
};


